
import java.util.List;

/**
 * @author TridevChaudhary
 *
 */
public class BulkModel {
	List<String> mobileNumbers;
	String message;
	
	public List<String> getMobileNumbers() {
		return mobileNumbers;
	}
	public void setMobileNumbers(List<String> mobileNumbers) {
		this.mobileNumbers = mobileNumbers;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessage() {
		return message;
	}

}