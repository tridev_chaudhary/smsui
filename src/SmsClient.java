

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SmsClient {

	/**
	 * @param currentData
	 * @return String
	 * 
	 */
	public static String getJsonObject(BulkModel currentData) {
		String json = null;
		try {
			Gson objGson = new GsonBuilder().setPrettyPrinting().create();
			BulkModel list1 = currentData;
			json = objGson.toJson(list1);
			return json;
		} catch (Exception e) {
			//TravelPdfGeneration.errorLogger.classLevLogDet(SmsClient.class.getSimpleName(), e);
			e.printStackTrace();
			return json;
		}

	}

	/**
	 * @param json
	 * @return String
	 * 
	 */
	public static String callSms(String json) {
		System.out.println(json);
		String transaction = null;
		System.out.println("..................................." + json);
		try {
			Client client = ClientBuilder.newClient(new ClientConfig().register(LoggingFilter.class));
			WebTarget webTarget = client.target("http://service-develb.godigit.com/DigitSMS/rest/sms/bulkDigit/");

			Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON);
			Response response = invocationBuilder.post(Entity.entity(json, MediaType.APPLICATION_JSON));

			if (response.getStatus() != 200) {
				System.err.println("Cannot send sms");
				return "Cannot send sms";
			}
			transaction = response.readEntity(String.class);
			return "Suceess";
		} catch (Exception e) {
		//	TravelPdfGeneration.errorLogger.classLevLogDet(SmsClient.class.getSimpleName(), e);
			e.printStackTrace();
			return "Failed";
		}

	}

	/**
	 * @param birt
	 * @return String
	 * 
	 */
	public static String sendSms(String mobile,String message) {

		String transaction = null;
		try {
			BulkModel sm = new BulkModel();
			
			mobile = ("91" + mobile.substring(mobile.length() - 10, mobile.length()));
			List<String> mobileNumbers = new ArrayList<String>();
			mobileNumbers.add(mobile);
			
			sm.setMobileNumbers(mobileNumbers);
			sm.setMessage(message);
			
			String json = getJsonObject(sm);
			transaction = callSms(json);
			return transaction;
		} catch (Exception e) {
		//	TravelPdfGeneration.errorLogger.classLevLogDet(SmsClient.class.getSimpleName(), e);
			e.printStackTrace();
			return transaction;
		}

	}
}
